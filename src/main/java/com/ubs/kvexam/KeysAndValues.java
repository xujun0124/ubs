package com.ubs.kvexam;

public interface KeysAndValues {

	void accept(String kvPairs);

	String display();
}
