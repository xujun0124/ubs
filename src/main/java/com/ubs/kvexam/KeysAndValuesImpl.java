package com.ubs.kvexam;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author xuj25
 * 
 *         Designed against UBS test requirements. Not considering the
 *         thread-safety in this version
 */

public class KeysAndValuesImpl implements KeysAndValues {

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private Logger logger = LoggerFactory.getLogger(KeysAndValuesImpl.class);
	private Map<String, Object> map = null;

	private Set<String> group = null;
	private Map<String, Boolean> occurrence = null;
	private ErrorListener listener = null;

	public KeysAndValuesImpl() {
		super();
		// Keys are sorted (alpha-ascending, case-insensitive)
		map = new TreeMap<String, Object>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.toLowerCase().compareTo(o2.toLowerCase());
			}
		});
	}

	public void reset() {
		map.clear();
	}

	@Override
	public void accept(String kvPairs) {
		logger.info("Accepting kv pair: {}", kvPairs);
		String[] pairs = kvPairs.split(",");
		for (String pair : pairs) {
			String[] kv = pair.split("=");
			String key = kv[0].trim();
			String strValue = kv[1].trim();

			if (group != null && group.contains(key)) {
				checkAtomicGroup(key);
			}

			Object result = map.get(key);
			Integer intValue = null;
			try {
				intValue = Integer.valueOf(strValue);
			} catch (NumberFormatException nfe) {
				map.put(key, strValue);
				logger.debug("Added a new key-value pair or overwrite key '{}', becasue new value '{}' is non-integer.", key, strValue);
				continue;
			}

			if (null != result && result instanceof Integer) {
				map.put(key, intValue + (Integer) result);
				logger.debug("Updated existing key {}, accumulate the original integer '{}' and new integer '{}'.", key, result, intValue);
			} else {
				try {
					map.put(key, Integer.valueOf(strValue));
					logger.debug("Added a new key-value pair as key '{}' and integer value '{}'.", key, strValue);
				} catch (NumberFormatException nfe) {
					map.put(key, strValue);
					logger.debug("Added a new key-value pair as key '{}' and string value '{}'.", key, strValue);
				}
			}
		}
		
		if (group != null) {
			checkAtomicGroup(null);
		}
	}

	private void checkAtomicGroup(String key) {
		Set<String> missing = getMissingGroupMember();
		if (null == key ) {
			if(missing.size() > 0){
				logger.warn("Missing the occurrence of {} in atomic group!", missing);
				listener.onIncompleteAtomicGroup(group, missing);
			}
		} else if (occurrence.get(key)) {
			if (missing.size() == 0) {
				logger.debug("Resetting the occurrence of keys {} in the atomic group.", group);
				for (Entry<String, Boolean> entry : occurrence.entrySet()) {
					entry.setValue(false);
				}
				occurrence.put(key, true);
			} else {
				logger.warn("Missing the occurrence of {} in atomic group!", missing);
				listener.onIncompleteAtomicGroup(group, missing);
			}
		} else {
			logger.warn("Mark the occurrence of key '{}' in atomic group!", key);
			occurrence.put(key, true);
		}
	}

	private Set<String> getMissingGroupMember() {
		Set<String> missing = new HashSet<String>();
		for (Entry<String, Boolean> entry : occurrence.entrySet()) {
			if (entry.getValue() == false) {
				missing.add(entry.getKey());
			}
		}
		return missing;
	}

	@Override
	public String display() {
		logger.info("Display map: {}", map);
		StringBuilder sb = new StringBuilder("");
		boolean start = true;
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			logger.debug("key: {}, value: {}", entry.getKey(), entry.getValue());
			if (start) {
				start = false;
			} else {
				sb.append(LINE_SEPARATOR);
			}
			sb.append(entry.getKey()).append("=").append(entry.getValue());
		}
		return sb.toString();
	}

	public Set<String> getGroup() {
		return group;
	}

	public void setGroup(Set<String> group) {
		this.group = group;
		if(null != group){
			occurrence = new HashMap<String, Boolean>();
			for (String key : group) {
				occurrence.put(key, false);
			}
		}
	}

	public ErrorListener getListener() {
		return listener;
	}

	public void setListener(ErrorListener listener) {
		this.listener = listener;
	}

}
