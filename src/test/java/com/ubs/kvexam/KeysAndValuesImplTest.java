package com.ubs.kvexam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KeysAndValuesImplTest {

	private static Logger logger = LoggerFactory.getLogger(KeysAndValuesImplTest.class);
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	private static KeysAndValuesImpl keysAndValuesImpl = null;
	private static Set<String> group = null;
	ErrorListener listener = Mockito.mock(ErrorListener.class);
	
	@Captor
	ArgumentCaptor<Set<String>> setArguCaptor1;
	
	@Captor
	ArgumentCaptor<Set<String>> setArguCaptor2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		logger.info("Initializing KeysAndValuesImpl...");
		keysAndValuesImpl = new KeysAndValuesImpl();
		group = new HashSet<String>();
		group.add("441");
		group.add("442");
		group.add("500");
		keysAndValuesImpl.setListener(new ErrorListener() {
			@Override
			public void onError(String msg) {
				logger.error(msg);
			}

			@Override
			public void onError(String msg, Exception e) {
				logger.error(msg, e);
			}

			@Override
			public void onIncompleteAtomicGroup(Set<String> group, Set<String> missing) {
				logger.error("Missing the occurrence of {} in atomic group {}!", missing, group);
			}
		});
		logger.info("====== Finished setUpBeforeClass() ======");
	}

	@Before
	public void beforeTest() throws Exception {
		logger.info("Resetting key-value map before each test.");
		keysAndValuesImpl.reset();
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void afterTest() throws Exception {
		logger.info("----------- TestCase Seperator -----------");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		logger.info("====== tearDownAfterClass(): finished all tests ====== ");
	}

	@Test
	public void test_01_acceptAndDisplay_sort() {
		logger.info("test_01_acceptAndDisplay_sort()");
		List<String> kvList = Arrays.asList(new String[] { "one=two", "Three=four", "5=6", "14=X" });
		StringBuilder expected = new StringBuilder("");
		expected.append("14=X").append(LINE_SEPARATOR).append("5=6").append(LINE_SEPARATOR).append("one=two").append(LINE_SEPARATOR).append("Three=four");
		
		keysAndValuesImpl.setGroup(null);
		for (String kvString : kvList) {
			keysAndValuesImpl.accept(kvString);
		}
		assertEquals(expected.toString(), keysAndValuesImpl.display());
	}

	@Test
	public void test_02_acceptAndDisplay_trim() {
		logger.info("test_02_acceptAndDisplay_trim()");
		String kvString = "pi = 314159,  hello=world";
		StringBuilder expected = new StringBuilder("");
		expected.append("hello=world").append(LINE_SEPARATOR).append("pi=314159");
		
		keysAndValuesImpl.setGroup(null);
		keysAndValuesImpl.accept(kvString);
		assertEquals(expected.toString(), keysAndValuesImpl.display());
	}

	@Test
	public void test_03_acceptAndDisplay__mergeInteger() {
		logger.info("test_03_acceptAndDisplay__mergeInteger()");
		List<String> kvList = Arrays.asList(new String[] { "14=15", "A=B52", "dry=D.R.Y.", "14=7", "14=4", "dry=Don't Repeat Yourself" });
		StringBuilder expected = new StringBuilder("");
		expected.append("14=26").append(LINE_SEPARATOR).append("A=B52").append(LINE_SEPARATOR).append("dry=Don't Repeat Yourself");
		keysAndValuesImpl.setGroup(null);
		
		for (String kvString : kvList) {
			keysAndValuesImpl.accept(kvString);
		}
		assertEquals(expected.toString(), keysAndValuesImpl.display());
		keysAndValuesImpl.reset();
		keysAndValuesImpl.accept("14=15, 14=7,A=B52, 14 = 4, dry = Don't Repeat Yourself");
		assertEquals(expected.toString(), keysAndValuesImpl.display());
	}

	@Test
	public void test_11_atomicGroup() {
		logger.info("test_11_atomicGroup()");
		keysAndValuesImpl.setGroup(group);
		keysAndValuesImpl.setListener(listener);
		keysAndValuesImpl.accept("441=one,X=Y, 442=2,500=three");
		Mockito.verify(listener, Mockito.never()).onError(Mockito.anyString());
		Mockito.verify(listener, Mockito.never()).onError(Mockito.anyString(), Mockito.any(Exception.class));
		Mockito.verify(listener, Mockito.never()).onIncompleteAtomicGroup(Mockito.anySetOf(String.class), Mockito.anySetOf(String.class));
	}

	@Test
	public void test_12_atomicGroupTwice() {
		logger.info("test_12_atomicGroupTwice()");
		keysAndValuesImpl.setGroup(group);
		keysAndValuesImpl.setListener(listener);
		keysAndValuesImpl.accept("18=zzz,441=one,500=three,442=2,442= A,441 =3,35=D,500=ok  ");
		Mockito.verify(listener, Mockito.never()).onError(Mockito.anyString());
		Mockito.verify(listener, Mockito.never()).onError(Mockito.anyString(), Mockito.any(Exception.class));
		Mockito.verify(listener, Mockito.never()).onIncompleteAtomicGroup(Mockito.anySetOf(String.class), Mockito.anySetOf(String.class));
	}

	@Test
	public void test_13_incompleteAtomicGroup() {
		logger.info("test_13_incompleteAtomicGroup()");
		keysAndValuesImpl.setGroup(group);
		keysAndValuesImpl.setListener(listener);
		keysAndValuesImpl.accept("441=3,500=not ok,13=qwerty");
		Mockito.verify(listener, Mockito.times(1)).onIncompleteAtomicGroup(setArguCaptor1.capture(), setArguCaptor2.capture());
		assertEquals(3, setArguCaptor1.getValue().size());
		assertEquals(1, setArguCaptor2.getValue().size());
		assertTrue(setArguCaptor2.getValue().contains("442"));
	}

	@Test
	public void test_14_secondGroupIncomplete() {
		logger.info("testSecondGroupIncomplete()");
		keysAndValuesImpl.setGroup(group);
		keysAndValuesImpl.setListener(listener);
		keysAndValuesImpl.accept("500= three , 6 = 7 ,441= one,442=1,442=4");
		Mockito.verify(listener, Mockito.times(1)).onIncompleteAtomicGroup(setArguCaptor1.capture(), setArguCaptor2.capture());
		assertEquals(3, setArguCaptor1.getValue().size());
		assertEquals(2, setArguCaptor2.getValue().size());
		assertTrue(setArguCaptor2.getValue().contains("441"));
		assertTrue(setArguCaptor2.getValue().contains("500"));
	}


}
